---
title: "Getting started with OSGi"
description: "This guide will help you get started quickly and efficiently with OSGi technology"
date: 2018-04-05T16:09:45-04:00
#hide_page_title: true
layout: "single"
---

Welcome to the OSGi world! If you're new to OSGi or looking to refresh your knowledge, you're in the right place. This guide will help you get started quickly and efficiently with OSGi technology.

## What is OSGi?

OSGi (Open Service Gateway Initiative) is a modular approach and specification that allows developers to create robust, highly decoupled and dynamic applications in Java. There are various implementations of this specification. The open source options include [Apache Felix](https://felix.apache.org/), [Eclipse Equinox](https://www.eclipse.org/equinox) and [Knopflerfish](http://www.knopflerfish.org/).

<!-- TODO [Eclipse Concierge](https://www.eclipse.org/concierge/) seems to be archived? If yes, then remove this comment. -->

**Why should I use it?**

- **Modularity**: Break your application into manageable pieces, making it easier to maintain and evolve.
- **Dynamic Updates**: Update or replace modules (known as **bundles**) in a running system without a JVM restart.
- **Service-Oriented**: Promotes a service-oriented architecture where components communicate through well-defined interfaces.
- **Isolated Classloaders**: Each OSGi bundle operates with its own classloader, ensuring that module dependencies are cleanly separated and preventing unintended interference between bundles.

[Read more about "What is OSGi?"](/resources/what-is-osgi/)

## Tutorials

If you are developer and want to start by writing code then here are some tutorials to help you.

<hr />

**[OSGi enRoute tutorials](https://enroute.osgi.org/):** A collection of tutorials covering the basics, building microservices, persistence and a FAQ. 
{{< details "Click for more information" >}}
[![OSGi enRoute](/images/resources/OSGi-enRoute-for-Web.png)](https://enroute.osgi.org/)

OSGi enRoute is a project that provides a quick start for developers new to OSGi. Offering simple steps for you to get results fast it allows you to see how easy it can be to create stand-alone OSGi applications. The objective of enRoute is to give you a solid understanding of the basics of OSGi and provide an onramp for you to build modern, modular, maintainable software applications and systems.

enRoute is based on the OSGi R7 specifications and Reference Implementations and offers an opinionated approach to OSGi focusing on Declarative Services (DS) and a Maven based tool chain.

Until recently OSGi examples and tools exposed too much infrastructure to new developers and this made OSGi hard to get started with. In addition there are way too many outdated and sometimes incorrect OSGi tutorials available online. Please don’t use these. OSGi enRoute is the goto place to get stated.

{{< /details >}}

**Tutorials written by individuals:** A collection of OSGi tutorials written by several individuals covering different topics.

- **Dirk Fauth**
  - [OSGi - bundles / fragments / dependencies](https://vogella.com/blog/osgi-bundles-fragments-dependencies/)
  - [Getting Started with OSGi Declarative Services](https://vogella.com/blog/getting-started-with-osgi-declarative-services-2024/)
  - [OSGi Component Testing](https://vogella.com/blog/osgi-component-testing-2024/)
  - [Configuring OSGi Declarative Services](https://vogella.com/blog/configuring-osgi-declarative-services-2024/)
  - [Control OSGi DS Component Instances](https://vogella.com/blog/control-osgi-ds-component-instances-2024/)
  - [Access OSGi Services via web interface](https://vogella.com/blog/access-osgi-services-via-web-interface-2024/)
  - [OSGi Event Admin - Publish &amp; Subscribe](https://vogella.com/blog/osgi-event-admin-publish-subscribe-2024/)
  - [Getting Started with OSGi Remote Services - Bndtools Edition](https://vogella.com/blog/getting-started-with-osgi-remote-services-bndtools-edition/)
  - [Getting Started with OSGi Remote Services - enRoute Maven Archetype Edition](https://vogella.com/blog/getting-started-with-osgi-remote-services-enroute-maven-archetype-edition/)
  - [Getting Started with OSGi Remote Services - PDE Edition](https://vogella.com/blog/getting-started-with-osgi-remote-services-pde-edition/)
  - [Build REST services with the OSGi Whiteboard Specification for Jakarta™ RESTful Web Services](https://vogella.com/blog/build-rest-services-with-osgi-jakarta-rs-whiteboard/)
  - [CRaCin` your OSGi application - start so fast I want to CRIU](https://vogella.com/blog/cracin-your-osgi-application/)

<!-- TODO add more tutorials-->

## An Experienced OSGi Developer?

If you are an experienced OSGi developer wanting to use more advanced or specific features or capabilities or find out about new upcoming specifications then see the link below:

- Forum: If you have questions about OSGi technology, or want to discuss thoughts and ideas then checkout the [Forum for Bnd/Bndtools and OSGi](https://bnd.discourse.group/).

- Alternatively there is an [OSGi users list](https://accounts.eclipse.org/mailing-list/osgi-users) you can use. This list is closely watched and contributed to by many of the OSGi experts and OSGi community so its a great place to get involved. Note, that the forum might replace the list in the future.

- [Bnd/Bndtools](https://bndtools.org/) ([github](https://github.com/bndtools/bnd)) is a really useful tool to familiarize yourself with if you already have some OSGi experience. Bnd/Bndtools includes Eclipse, Maven, and Gradle plugins. Bnd is the engine behind a number of popular software development tools that support OSGi. Bndtools is also worth exploring as a plugin to Eclipse that makes working with OSGi much easier.

- The OSGi specifications are open and [freely available](https://docs.osgi.org/specification/) for your review.

- The technical work of specification development is done by the [OSGi Specification Project](https://projects.eclipse.org/projects/technology.osgi). Please consider getting involved with the project.

- Try out some of the different OSGi framework implementations. There are a number of commercial and open source options. The open source options include [Apache Felix](https://felix.apache.org/), [Eclipse Equinox](https://www.eclipse.org/equinox), [Knopflerfish](http://www.knopflerfish.org/).

- You can find out more by following the [OSGi Blog](https://blog.osgi.org/) or [OSGi Twitter account](https://twitter.com/OSGiWG), or by joining the [OSGi LinkedIn Group](https://www.linkedin.com/groups/122461/).

## How can I contribute?

Contributions are always welcome.

- **To this website:** There is a Git repository at [https://gitlab.eclipse.org/eclipsefdn/it/websites/osgi.org](osgi.org) with instructions how to contribute content and changes.
- **To other parts of the OSGi ecosystem:** Under [https://github.com/osgi/](github.com/osgi/) you can find all the repositories where you can contribute. 
- **Become a member of the OSGi Working Group:** On our [About us](/about/) page, you will find our Charter, containing information about how to become a member.

Do not hesitate to ask in the [Forum](https://bnd.discourse.group/) if you have a question about contributing.

## Other Useful Information

- [OSGi Documentation](https://docs.osgi.org/): specifications, javadoc, whitepapers
- [OSGi on GitHub](https://github.com/osgi/)
- [OSGi Working Group](https://www.eclipse.org/org/workinggroups/osgi-charter.php) at Eclipse
