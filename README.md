# osgi.org

The [https://www.osgi.org](https://www.osgi.org) website is generated with [Hugo](https://gohugo.io/documentation/).

The OSGi WG is a worldwide consortium of technology innovators that advances a proven and mature process to create open specifications that enable the modular assembly of software built with Java technology. Modularity reduces software complexity; OSGi is the best model to modularize Java.

[![Build Status](https://foundation.eclipse.org/ci/webdev/buildStatus/icon?job=public%2Fosgi.org%2Fmain)](https://foundation.eclipse.org/ci/webdev/job/public/job/osgi.org/job/main/)

## Getting started

Install dependencies, build assets and start a webserver:

```bash
yarn && hugo server
```

You can find guidance on the page-level metadata [here](https://eclipsefdn-hugo-solstice-theme.netlify.app/), and examples of the assorted page types (components) [here](https://eclipsefdn-hugo-solstice-theme.netlify.app/components/).

## Contributing

1. [Fork](https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html) the [osgi.org](https://gitlab.eclipse.org/eclipsefdn/it/websites/osgi.org) repository
2. Clone repository: `git clone https://gitlab.eclipse.org/[your_eclipsefdn_username]/osgi.org.git`
3. Create your feature branch: `git checkout -b my-new-feature`
4. Commit your changes: `git commit -m 'Add some feature' -s`
5. Push feature branch: `git push origin my-new-feature`
6. Submit a merge request

### Declared Project Licenses

This program and the accompanying materials are made available under the terms
of the Eclipse Public License v. 2.0 which is available at
<https://www.eclipse.org/legal/epl-2.0>.

SPDX-License-Identifier: EPL-2.0

## Related projects

### [EclipseFdn/solstice-assets](https://gitlab.eclipse.org/eclipsefdn/it/webdev/solstice-assets)

Images, less and JavaScript files for the Eclipse Foundation look and feel.

### [EclipseFdn/hugo-solstice-theme](https://gitlab.eclipse.org/eclipsefdn/it/webdev/hugo-solstice-theme/)

Hugo theme of the Eclipse Foundation look and feel. 

## Bugs and feature requests

Have a bug or a feature request? Please search for existing and closed issues. If your problem or idea is not addressed yet, [please open a new issue](https://gitlab.eclipse.org/eclipsefdn/it/websites/osgi.org/-/issues).

## Author

**Christopher Guindon (Eclipse Foundation)**

- <https://twitter.com/chrisguindon>
- <https://github.com/chrisguindon>

## Trademarks

- Eclipse® is a Trademark of the Eclipse Foundation, Inc.
- Eclipse Foundation is a Trademark of the Eclipse Foundation, Inc.

## Copyright and license

Copyright 2021-2022 the [Eclipse Foundation, Inc.](https://www.eclipse.org) and the [osgi.org authors](https://gitlab.eclipse.org/eclipsefdn/it/websites/osgi.org/-/graphs/master). Code released under the [Eclipse Public License Version 2.0 (EPL-2.0)](https://gitlab.eclipse.org/eclipsefdn/it/websites/osgi.org/-/raw/master/README.md).
